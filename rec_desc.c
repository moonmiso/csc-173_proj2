/*
 * File: rec_desc.c
 *Creator: Muskaan Mendiratta
 *Created Tue Oct 9 2018
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tree.h"
#include "rec_desc.h"

char *nextInputCharacter;


/**
 *Grammar for digits
 *<D> = 0|1|2|3|4|5|6|7|8|9
 */
extern Tree D(){
  //0
  if (*nextInputCharacter == '0'){
    nextInputCharacter++;
    return Tree_makeNode1('D', Tree_makeNode0('0'));
  }
  //1
  if (*nextInputCharacter == '1'){
    nextInputCharacter++;
    return Tree_makeNode1('D', Tree_makeNode0('1'));
  }
  //2
  if (*nextInputCharacter == '2'){
    nextInputCharacter++;
    return Tree_makeNode1('D', Tree_makeNode0('2'));
  }
  //3
  if (*nextInputCharacter == '3'){
    nextInputCharacter++;
    return Tree_makeNode1('D', Tree_makeNode0('3'));
  }
  //4
  if (*nextInputCharacter == '4'){
    nextInputCharacter++;
    return Tree_makeNode1('D', Tree_makeNode0('4'));
  }
  //5
  if (*nextInputCharacter == '5'){
    nextInputCharacter++;
    return Tree_makeNode1('D', Tree_makeNode0('5'));
  }
  //6
  if (*nextInputCharacter == '6'){
    nextInputCharacter++;
    return Tree_makeNode1('D', Tree_makeNode0('6'));
  }
  //7
  if (*nextInputCharacter == '7'){
    nextInputCharacter++;
    return Tree_makeNode1('D', Tree_makeNode0('7'));
  }
  //8
  if (*nextInputCharacter == '8'){
    nextInputCharacter++;
    return Tree_makeNode1('D', Tree_makeNode0('8'));
  }
  //9
  if (*nextInputCharacter == '9'){
    nextInputCharacter++;
    return Tree_makeNode1('D', Tree_makeNode0('9'));
  }
  return NULL;
}

/**
 *Returns true if character is +,-,*, /,(,),\0
 *Yes, it returns true even for \0 
*/
extern bool isOperator(){
  if (*nextInputCharacter == '+'|| *nextInputCharacter == '-'||
      *nextInputCharacter == '*'|| *nextInputCharacter == '/'||
      *nextInputCharacter == '('|| *nextInputCharacter == ')'||
      *nextInputCharacter == '\0'){
    return true;
  }
  return false;
}


/**
 *Grammar for numbers
 *<N> -> <D><N>
 *<N*> -> <N> | e
 */

extern Tree Nstar(){
  Tree number = N();
  if (number != NULL){
    return Tree_makeNode1('X',number);
  }else{
    return Tree_makeNode1('X', Tree_makeNode0('e'));
  }
}

extern Tree N(){
  //check if it is a digit
  Tree digit = D(*nextInputCharacter);
  if (digit != NULL){
    //check if it is a number
    Tree number = Nstar(*nextInputCharacter);
    if (number != NULL){
      return Tree_makeNode2('N', digit, number);
    }else{
      // second production
      return Tree_makeNode1('N', digit);
    }
  }
  return NULL;
}

/**
 *Grammar for Factor <F>
 *<F> -> (<E>) | <N>
 *Reference: Page 620, FOCS Aho and Ullman
 */
extern Tree F (){
  //match first production
  if (*nextInputCharacter == '('){
    nextInputCharacter++;
    Tree expression = E(nextInputCharacter);
    if (expression != NULL && *nextInputCharacter == ')'){
      nextInputCharacter++;
      return Tree_makeNode3('F', Tree_makeNode0('('), expression, Tree_makeNode0(')'));
    }
  }else{
    //match second production
    Tree number = N("nextInputCharacter");
    if (number != NULL){
      return Tree_makeNode1('F', number);
    }
  }
  //if none of the productions match
  return NULL;
  
}


/**
 *Grammar for Factor* <F*>
 *<F*> -> *<F><F*> | /<F><F*> | e
 *For the purpose of the code, I will rename F* as A in the tree representation
 */
extern Tree Fstar(){
  if (*nextInputCharacter == '*'){
    //first production
    nextInputCharacter++;
    Tree factor = F(*nextInputCharacter);
    if (factor != NULL){
      Tree factor_star = Fstar(*nextInputCharacter);
      if (factor_star != NULL){
	return Tree_makeNode3('A', Tree_makeNode0('*'), factor, factor_star);
      }
    }
  }
  else if (*nextInputCharacter == '/'){
    //second production
    nextInputCharacter++;
    Tree factor = F(*nextInputCharacter);
    if (factor != NULL){
      Tree factor_star = Fstar(*nextInputCharacter);
      if (factor_star != NULL){
	return Tree_makeNode3('A', Tree_makeNode0('/'), factor, factor_star);
      }else{
	return NULL;
      }
    }
  }
  else{
    //third production, match with epsilon
    return Tree_makeNode1('A', Tree_makeNode0('e'));
  }
  return NULL;
}

/**
 *Grammar for T
 *<T> -> <F><F*>
 */
extern Tree T(){
  Tree factor = F(*nextInputCharacter);
  if (factor != NULL){
    Tree factor_star = Fstar(nextInputCharacter);
    if (factor_star != NULL){
      return Tree_makeNode2('T', factor, factor_star);
    }
  }
  return NULL;
}

/**
 *Grammar for T*
 *<T*> = +<T><T*> | -<T><T*> | e
 *I will be renaming T* as B in the tree representation
 */
extern Tree Tstar(){
  if (*nextInputCharacter == '+'){
    nextInputCharacter++;
    Tree term = T(*nextInputCharacter);
    if (term != NULL){
      Tree term_star = Tstar(*nextInputCharacter);
      if (term_star != NULL){
	return Tree_makeNode3('B', Tree_makeNode0('+'), term, term_star);
      }
    }
  }
  else if (*nextInputCharacter == '-'){
    nextInputCharacter++;
    Tree term = T(*nextInputCharacter);
    if (term != NULL){
      Tree term_star = Tstar(*nextInputCharacter);
      if (term_star != NULL){
	return Tree_makeNode3('B', Tree_makeNode0('-'), term, term_star);
      }
    }
  }
  else {
    return Tree_makeNode1('B', Tree_makeNode0('e'));    
  }
  return NULL;
}

/**
 *Grammar for E
 *<E> -> <T><T*>
 */
extern Tree E(){  
  Tree term = T(*nextInputCharacter);
  if (term != NULL){
       
    Tree term_star = Tstar(nextInputCharacter);
    if (term_star != NULL){
       
      return Tree_makeNode2('E', term, term_star);
    }
  }
  return NULL;
}
				       
  


