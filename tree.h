/*
 *File: tree.h
 *Creator: Muskaan Mendiratta
 *Created Wed Oct 10 2018
 */

#ifndef _tree_h
#define _tree__h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Tree *Tree;


/**
 *Allocate and return a new Tree with the given head
 */
extern Tree new_Tree(char c);

/**
 *Free the given tree
 */
extern void Tree_free(Tree tree);


/**
 *Make a Tree with one node and no children
 */
extern Tree Tree_makeNode0(char c);

/**
 *Make a Tree with one child
 */
extern Tree Tree_makeNode1(char c, Tree t);

/**
 *Make a Tree with two children
 */
extern Tree Tree_makeNode2(char c, Tree t1, Tree t2);

/**
 *Make a Tree with three children
 */
extern Tree Tree_makeNode3(char c, Tree t1, Tree t2, Tree t3);

/**
 *Make a Tree with four children
 */
extern Tree Tree_makeNode4(char c, Tree t1, Tree t2, Tree t3, Tree t4);

/**
 *Returns the char value of the tree
 */
extern char Tree_getValue(Tree tree);

/**
 *Returns the left child of the tree
 */
extern Tree Tree_leftChild(Tree tree);

/**
 *Returns the right child of the tree
 */
extern Tree Tree_rightSibling(Tree tree);

/**
 *Print given Tree with counter passed through reference and another counter to keep track of indentation
 */
extern void Tree_print(Tree tree, int start, int indent);     
      
#endif
