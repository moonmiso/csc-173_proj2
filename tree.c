/*
 *File: tree.c
 *Creator: Muskaan Mendiratta
 *Created Wed Oct 10 2018
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tree.h"

/*
 *Tree struct
 */
struct Tree{
  char value;  
  Tree leftChild;
  Tree rightSibling;  
};

/*Allocate and return tree with given value
 */

extern Tree new_Tree(char c){
  Tree this = (Tree)malloc(sizeof(struct Tree));
  if (this == NULL){
    return NULL; //Out of memory
  }
  this->value = c;
  this->leftChild = NULL;
  this->rightSibling = NULL;
  return this;
}

extern void Tree_free (Tree tree){
  free(tree);
}

extern Tree Tree_makeNode0(char c){
  Tree tree = new_Tree(c);
  return tree;
}

extern Tree Tree_makeNode1(char c, Tree t1){
  Tree tree = Tree_makeNode0(c);
  tree->leftChild=t1;
  return tree;
}

extern Tree Tree_makeNode2(char c, Tree t1, Tree t2){
  Tree tree = Tree_makeNode1(c, t1);
  t1->rightSibling = t2;
  return tree;
}

extern Tree Tree_makeNode3(char c, Tree t1, Tree t2, Tree t3){
  Tree tree = Tree_makeNode1(c,t1);
  t1->rightSibling = t2;
  t2->rightSibling = t3;
  return tree;
}

extern Tree Tree_makeNode4(char c, Tree t1, Tree t2, Tree t3, Tree t4){
  Tree tree = Tree_makeNode1(c, t1);
  t1->rightSibling = t2;
  t2->rightSibling = t3;
  t3->rightSibling = t4;
  return tree;
}

extern char Tree_getValue(Tree tree){
  return tree->value;
}

extern Tree Tree_leftChild(Tree tree){
  return tree->leftChild;
}

extern Tree Tree_rightSibling(Tree tree){
  return tree->rightSibling;
}

extern void Tree_print(Tree tree, int start, int numIndented) {
    if (tree == NULL && start == 1) {
        printf("This expression cannot be parsed");
    } else {
        printf("\n");
        for (int i = 0; i < numIndented; i++) {
            printf("\t");
        }
        printf("%c", tree->value);
        
        if (tree->leftChild != NULL) {
            Tree_print(tree->leftChild, 0, numIndented+1);
        }
        
        if (tree->rightSibling != NULL) {
            Tree_print(tree->rightSibling, 0, numIndented);
        }
    }
}
