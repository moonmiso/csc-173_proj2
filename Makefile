
# File: Makefile
# Creator: Muskaan Mendiratta
# Created: Sun Oct 14 2018
# CSC 173 Project 2

PROGRAMS = auto 

CFLAGS = -g -std=c99 -Wall -Werror

programs: $(PROGRAMS)

auto: stack.o tree.o rec_desc.o table_driven.o 
	$(CC) -o $@ $^

clean:
	-rm $(PROGRAMS) *.o
	-rm -r *.dSYM
