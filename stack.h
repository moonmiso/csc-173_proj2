/*
 *File: stack.h
 *Creator: Muskaan Mendiratta
 *Created: Wed Oct 10 2018
 */

#ifndef _stack_h
#define _stack_h


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct Stack *Stack;

/**
 *Allocate and return a new Stack
 */
extern Stack new_Stack();

/**
 *Free the given stack
 */
extern void Stack_free(Stack stack);

/**
 *Check if stack is full
 */
extern bool Stack_isFull(Stack stack);

/**
 *Check if stack is empty
 */
extern bool Stack_isEmpty(Stack stack);

/**
 *Push a character on to the stack
 */
extern void Stack_push(Stack stack, char c);

/**
 *Pop a character from the stack
 */
extern char Stack_pop(Stack stack);

#endif
