CSC173_Proj2
Name: Muskaan Mendiratta
Assignment Number: Project 02, Unit 02
I did not collaborate with anyone on this project.

Files:
stack.h		Header file 
stack.c		Contains data structure implementation for a stack
tree.h		Header file
tree.c		Contains data structure implementation for an tree
rec_desc.h	Header file
rec_desc.c	Contains methods for recursive descent parsing
table_driven.h	Header file
table_driven.c	Contains int calculate() and int eval() functions for evaluating expressions, as well as functions for table driven parsing

auto		Executable file
Makefile	To compile


Compiling and executing instructions:
I have included all files in my Makefile.
Steps for compiling and executing:

$ make
$ ./auto

Grammar:
<D>  -> 0|1|2|3|4|5|6|7|8|9
<N>  -> <D><N*>
<N*> -> <N>				N* will be printed out as X
<N*> -> e
<F>  -> (<E>)
<F>  -> <N>
<F*> -> *<F><F*>			F* will be printed out as A
<F*> -> /<F><F*>
<F*> -> e
<T>  -> <F><F*>
<T*> -> +<T><T*>			T* will be printed out as B
<T*> -> -<T><T*>
<T*> -> e
<E>  -> <T><T*>

List of productions (used in the parser table for my table driven parser):
1	'1'
2	'2'
3	'3'
4	'4'
5	'5'
6	'6'
7	'7'
8	'8'
9	'9'
10	'10'
11	<N>  -> <D><N*>
12	<N*> -> <N>
13	<N*> -> e
14	<F>  -> (<E>)
15	<F>  -> <N>
16	<F*> -> *<F><F*>
17	<F*> -> /<F><F*>
18	<F*> -> e
19	<T>  -> <F><F*>
20	<T*> -> +<T><T*>
21	<T*> -> -<T><T*>
22	<T*> -> e
23	<E>  -> <T><T*>

Parse table:

		+	-	*	/	(	)	1	2	3	4	5	6	7	8	9	0	ENDM

E		-1	-1	-1	-1	23	-1	23	23	23	23	23	23	23	23	23	23	-1

B/T*		20	21	-1	-1	-1	22	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	14

T		19	-1	-1	-1	-1	-1	19	19	19	19	19	19	19	19	19	19	-1

A/F*		22	22	16	17	-1	22	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	22

F		-1	-1	-1	-1	14	-1	15	15	15	15	15	15	15	15	15	15	-1

X/N*		-1	-1	-1	-1	-1	13	12	12	12	12	12	12	12	12	12	12	-1

N		-1	-1	-1	-1	-1	-1	11	11	11	11	11	11	11	11	11	11	-1

D		-1	-1	-1	-1	-1	-1	10	10	10	10	10	10	10	10	10	10	-1



Project submission PDF:
I used -std=c99 -Wall -Werror
I used valgrind but did not get a clean report
(My PDF editor didn't let me check a box, so I left the boxes unchecked
