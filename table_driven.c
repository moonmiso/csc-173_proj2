/*
 * File: table_driven.c
 *Creator: Muskaan Mendiratta
 *Created Thur Oct 11 2018
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tree.h"
#include "rec_desc.h"
#include "stack.h"
#include "table_driven.h"


int parse_table [8][17] =
  {
   /* E */  {-1, -1, -1, -1, 23, -1, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, -1},
   /* T* */ {20, 21, -1, -1, -1, 22, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 23},
   /* T */  {19, -1, -1, -1, -1, -1, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, -1},
   /* F* */ {22, 22, 16, 17, -1, 22, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 22},
   /* F */  {-1, -1, -1, -1, 14, -1, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, -1},
   /* N* */ {-1, -1, -1, -1, -1, 13, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 13},
   /* N */  {-1, -1, -1, -1, -1, -1, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, -1},
   /* D */  {-1, -1, -1, -1, -1, -1, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, -1}
  };

char* nextInputCharacter;
Tree tree;  
extern bool isDigit(char c){
  if (c == '1' ||c == '2' ||
      c == '3' ||c == '4' ||
      c == '5' ||c == '6' ||
      c == '7' ||c == '8' ||
      c == '9' ||c == '0'){
    return true;
  }
  return false;
}

extern bool isOp(char c){
  if(c == '+' ||c == '-' ||
     c == '*' ||c == '/'){
    return true;
  }
  return false;
}

/**
 *Function for evaluating expressions
 *Sorry this is not in a different file T.T
 */
extern int calculate (Tree tree){
  char str[128];
  char c = Tree_getValue(tree);
  
  switch(c){
  case '0':
    return 0;
  case '1':
    return 1;
  case '2':
    return 2;
  case '3':
    return 3;
  case '4':
    return 4;
  case '5':
    return 5;
  case '6':
    return 6;
  case '7':
    return 7;
  case '8':
    return 8;
  case '9':
    return 9;
  case 'N':
    return calculate(Tree_leftChild(tree));
  case 'X':
    return calculate(Tree_leftChild(tree));
  case 'D':
    if (Tree_getValue(Tree_leftChild(Tree_rightSibling(tree))) == 'e'){
      return calculate(Tree_leftChild(tree));
    }
    else{
      //It is a double digit number
      sprintf(str, "%d%d", calculate(Tree_leftChild(tree)), calculate(Tree_rightSibling(tree)));
      return atoi(str);
    }
  case 'T':
     //if <T*> -> e
     if (!isOp(Tree_getValue(Tree_leftChild(Tree_rightSibling(tree))))){
       return calculate(Tree_leftChild(tree));
     }
    
     if (Tree_getValue(Tree_leftChild(Tree_rightSibling(tree))) == '+'){
       //<T*> -> +<T><T*>
       return calculate(Tree_leftChild(tree)) + calculate(Tree_rightSibling
							  (Tree_leftChild(Tree_rightSibling(tree))));
     }
     if (Tree_getValue(Tree_leftChild(Tree_rightSibling(tree))) == '-'){
       //<T*> -> -<T><T*>
       return calculate(Tree_leftChild(tree)) - calculate(Tree_rightSibling
							  (Tree_leftChild(Tree_rightSibling(tree))));
     }
     return 0; 
  case 'B':
    return calculate(Tree_leftChild(tree));
  case 'F':
    if(Tree_getValue(Tree_leftChild(tree)) == '('){
      //<F> -> (<E>)
      if(Tree_getValue(Tree_leftChild(Tree_rightSibling(tree))) == 'e'){
	//<F*> -> e
	return calculate(Tree_rightSibling(Tree_leftChild(tree)));
      }
      if(Tree_getValue(Tree_leftChild(Tree_rightSibling(tree))) == '*'){
	//<F*> -> *<F><F*>
	return (calculate(Tree_rightSibling(Tree_leftChild(tree)))) *
	  calculate(Tree_rightSibling(Tree_leftChild(Tree_rightSibling(tree))));
      }
      if(Tree_getValue(Tree_leftChild(Tree_rightSibling(tree))) == '/'){
	//<F*> -> /<F><F*>
	return (calculate(Tree_rightSibling(Tree_leftChild(tree)))) *
	  calculate(Tree_rightSibling(Tree_leftChild(Tree_rightSibling(tree))));
      }
    }
    //same execution as above, without parenthesis 
    if(Tree_getValue(Tree_leftChild(Tree_rightSibling(tree))) == 'e'){
      return calculate(Tree_leftChild(tree));
    }
    if(Tree_getValue(Tree_leftChild(Tree_rightSibling(tree))) == '*'){
      return (calculate(Tree_leftChild(tree)) *
	      calculate(Tree_rightSibling(Tree_leftChild(Tree_rightSibling(tree)))));
    }
    if(Tree_getValue(Tree_leftChild(Tree_rightSibling(tree))) == '/'){
      return (calculate(Tree_leftChild(tree)) /
	      calculate(Tree_rightSibling(Tree_leftChild(Tree_rightSibling(tree)))));
    }
    return 1; 
  case 'A':
    //<T*> -> e
    if (Tree_getValue(Tree_leftChild(tree)) == 'e'){
      return 0;
    }
    return calculate(Tree_leftChild(tree));
  case 'E':
    //<E> -> <T><T*>
    return calculate(Tree_leftChild(tree));

  default:
    break;
  }
  return 0;
}

extern int eval(Tree tree){
  return calculate(Tree_leftChild(tree));
}

extern int terminal(char c){
  switch(c){
  case '+':
    return 0;
  case '-':
    return 1;
  case '*':
    return 2;
  case '/':
    return 3;
  case '(':
    return 4;
  case ')':
    return 5;
  case '1':
    return 6;
  case '2':
    return 7;
  case '3':
    return 8;
  case '4':
    return 9;
  case '5':
    return 10;
  case '6':
    return 11;
  case '7':
    return 12;
  case'8':
    return 13;
  case '9':
    return 14;
  case '0':
    return 15;
  default:
    return -1;
  }
}

extern int syntactic_category(char c){
  switch(c){
  case 'E':
    return 0;
  case 'B':
    return 1;
  case 'T':
    return 2;
  case 'A':
    return 3;
  case 'F':
    return 4;
  case 'X':
    return 5;
  case 'N':
    return 6;
  case 'D':
    return 7;
  default:
    return -1;
  }
}

//---------------TABLE DRIVEN PARSER-----------------------
extern bool isTerminal(char c){
  if (c == '1' ||c == '2' ||c == '3' ||c == '4' ||c == '5' ||
      c == '6' ||c == '7' ||c == '8' ||c == '9' ||c == '0' ||
      c == '+' ||c == '-' ||c == '*' ||c == '/' ||c == '(' ||
      c == ')' ){
    return true;
  }
  return false;
}

/**
 *Gets production                                      
 */
extern Stack get_Production(int i){
  Stack stack = new_Stack();
  switch(i){
  case -1:
    return NULL;
  case 1:
    Stack_push(stack, '1');
    tree = D();
    return stack;
  case 2:
    Stack_push(stack, '2');
    tree = D();
    return stack;
  case 3:
    Stack_push(stack, '3');
    tree = D();
    return stack;
  case 4:
    Stack_push(stack, '4');
    tree = D();
    return stack;
  case 5:
    Stack_push(stack, '5');
    tree = D();
    return stack;
  case 6:
    Stack_push(stack, '6');
    tree = D();
    return stack;
  case 7:
    Stack_push(stack, '7');
    tree = D();
    return stack;
  case 8:
    Stack_push(stack, '8');
    tree = D();
    return stack;
  case 9:
    Stack_push(stack, '9');
    tree = D();
    return stack;
  case 10:
    Stack_push(stack, '0');
    tree = D();
    return stack;
  case 11:
    /* <N> -> <D><N*> */
    Stack_push(stack, 'X');
    Stack_push(stack, 'D');
    tree = N();
    return stack;
  case 12:
    /* <N*> -> <N>*/
    Stack_push(stack, 'N');
    tree = Nstar();
    return stack;
  case 13:
    /* <N*> -> e */
    Stack_push(stack, 'e');
    tree = Nstar();
    return stack;
  case 14:
    /* <F> -> (<E>) */
    Stack_push(stack, '(');
    Stack_push(stack, 'E');
    Stack_push(stack, ')');
    tree = F();
    return stack;
  case 15:
    /* <F> -> <N> */
    Stack_push(stack, 'N');
    tree = F();
    return stack;
  case 16:
    /* <F*> -> *<F><F*> */
    Stack_push(stack, 'A');
    Stack_push(stack, 'F');
    Stack_push(stack, '*');
    tree = Fstar();
    return stack;
  case 17:
    /*<F*> -> /<F><F*> */
    Stack_push(stack, 'A');
    Stack_push(stack, 'F');
    Stack_push(stack, '*');
    tree = Fstar();
    return stack;
  case 18:
    /* <F*> -> e */
    Stack_push(stack, 'e');
    tree = Fstar();
    return stack;
  case 19:
    /* <T> -> <F><F*> */
    Stack_push(stack, 'A');
    Stack_push(stack, 'F');
    tree = T();
    return stack;
  case 20:
    /* <T*> -> +<T><T*> */
    Stack_push(stack, 'B');
    Stack_push(stack, 'T');
    Stack_push(stack, '+');
    tree = Tstar();
    return stack;
  case 21:
    /* <T*> -> -<T><T*> */
    Stack_push(stack, 'B');
    Stack_push(stack, 'T');
    Stack_push(stack, '-');
    tree = Tstar();
    return stack;
  case 22:
    /* <T*> -> e */
    Stack_push(stack, 'e');
    tree = Tstar();
    return stack;
  case 23:
    /* <E> -> <T><T*> */
    Stack_push(stack, 'B');
    Stack_push(stack, 'T');
    tree = E();
    return stack;
  }
  return NULL;
}

char *nextInputCharacter;
extern Tree addProduction(Stack stack, char c){

  int production = parse_table[syntactic_category(c)][terminal(*nextInputCharacter)];
  if (terminal(*nextInputCharacter) == -1){
    production=0;
  }
  stack = get_Production(production);
  return tree;
}

extern Tree evaluate(){
  Stack stack = new_Stack();
  Stack_push(stack, 'E');
  while (!Stack_isEmpty (stack)){
    char c = Stack_pop(stack);
    if (isTerminal(c)){
      nextInputCharacter++;
    }else{
      tree = addProduction(stack, c);
    }
  }
  Stack_free(stack);
  return tree;
}

int main(){
  char expression[256];
  char num [256];

  nextInputCharacter = expression;
  Tree parseTree;
  Tree parseTree2;
  
  printf("%s\n", "Enter 1 for recursive descent parser, 2 for table driven parser, quit to exit");
  scanf("%s", num);
  printf("%s\n", "Type in an expression");
  scanf("%s", expression);
  printf("the expression you typed in was %s\n", expression);
  if(strcmp(num, "1") == 0){
    printf("%s\n", "----------------RECURSIVE DESCENT PARSER----------------");
    parseTree = evaluate();
    
    Tree_print(parseTree, 1, 0);
    printf("\n");
    printf("%s\n", "Evaluated function:");
    printf("%d\n", eval(parseTree));
    Tree_free(parseTree);
  }else if (strcmp(num,"2") == 0){    
    
    
    printf("%s\n", "----------------TABLE DRIVEN PARSER----------------");
    parseTree2 = evaluate();
    
    Tree_print(parseTree2, 1, 0);
    printf("\n");
    printf("%s\n", "Evaluated function:");
    printf("%d\n", eval(parseTree2));
  }else{
    printf("%s\n", "Invalid input");    
  }
}

    
    
      
  




