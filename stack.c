/*
 *File: stack.c
 *Creator: Muskaan Mendiratta
 *Created Wed Oct 10 2018
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "stack.h"

/*
 *Stack struct
 */
struct Stack{
  int top; //index of the top element in the array 
  int capacity;
  char* array; //data structure for my stack
};

extern Stack new_Stack(){
  Stack this = (Stack)malloc(sizeof(struct Stack));
  if (this == NULL){
    return NULL; //Out of memory
  }
  this->top = -1;//stack is initially empty
  this->capacity = 30; //stack will be full if it reaches the capacity of 30
  this->array = (char*)malloc(this->capacity * sizeof(char));
  return this;
}

extern void Stack_free(Stack stack){
  free(stack);
}

extern bool Stack_isFull(Stack stack){
  if (stack->top == stack->capacity-1){ 
    return true;
  }
  return false;
}

extern bool Stack_isEmpty(Stack stack){
  if (stack->top == -1){
    return true;
  }
  return false;
}

extern void Stack_push(Stack stack, char c){
  if (stack->top == stack->capacity-1){   //stack is full
    printf("%s\n", "Stack is full");
  }else{
    stack->top += 1;
    stack->array[stack->top] = c;
  }
}
  
extern char Stack_pop(Stack stack){
  if (stack->top == -1){
    printf("%s\n", "Stack is empty");
    return '$';
  }else{
    int num;
    num = stack->top;
    stack->top -= 1; //index of top element is decremented by 1
    return stack->array[num];
  }
}


    

				  
    
