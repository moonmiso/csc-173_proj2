/*
 * File: rec_desc.h
 *Creator: Muskaan Mendiratta
 *Created Tue Oct 9 2018
 */

#ifndef _rec_desc_h
#define _rec_desc_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "tree.h"


/**
 * Parses a digit and returns a Tree.
 */
extern Tree D();

/**
 *Returns true if guven character is an operator or '\0'
 */
extern bool isOperator();

/**
 *Parses for a number in given strong
 */
extern Tree N();
extern Tree Nstar();
/**
 *Parses for a factor in given string
 */
extern Tree F();
extern Tree Fstar();

/**
 *Parses for a term in given string
 */
extern Tree T();
extern Tree Tstar();

/**
 *Parses for an expression in given string
 */
extern Tree E();


#endif
