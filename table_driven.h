/*
 *File: table_drive.h
 *Creator: Muskaan Mendiratta
 *Created: Sat Oct 13 2018
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "stack.h"
#include "tree.h"
#include "rec_desc.h"

extern int terminal(char c);

extern int syntactic_category(char c);

extern bool isTerminal(char c);

extern Stack get_Production(int i);

extern Tree addProduction(Stack stack, char c);

extern Tree evaluate();

extern bool isDigit(char c);

extern bool isOp(char c);

extern int calculate(Tree tree);

extern int eval(Tree tree);
